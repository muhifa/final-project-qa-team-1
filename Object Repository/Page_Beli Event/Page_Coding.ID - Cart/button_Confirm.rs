<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Confirm</name>
   <tag></tag>
   <elementGuidId>ca983cf0-8d91-4b56-a34f-2a1ccab40a58</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#confirm</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//button[@id='confirm']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>3d3acbb2-7af2-45ed-9455-b465e4c53fba</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>7707b4c5-cc4f-4174-a0d3-8945ee0dc4b9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>confirm</value>
      <webElementGuid>9e010ec5-e74b-467a-9a1f-0f2624422aec</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-primary btn-order btn-block</value>
      <webElementGuid>4a18042d-4125-49b4-b80a-9b7e503826b2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Confirm</value>
      <webElementGuid>8c456b64-25af-4ce4-ae17-31df9bad79a0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;confirm&quot;)</value>
      <webElementGuid>288934be-d9f7-404d-872b-a82fc5b8c20b</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//button[@id='confirm']</value>
      <webElementGuid>615b378e-10a2-4d2d-95b1-3cb4a96a2c25</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='modalform']/button</value>
      <webElementGuid>00e9fd70-76e7-49af-8e84-b28b31df3a30</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Transfer QR Code / Bank (cek otomatis)'])[1]/following::button[1]</value>
      <webElementGuid>26e34f39-00e9-4122-ad85-7d3ff9b0df43</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Information'])[1]/preceding::button[1]</value>
      <webElementGuid>c9530de4-ac08-493b-8c6b-76a1d43dd71e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Ok'])[1]/preceding::button[2]</value>
      <webElementGuid>63804196-a314-46c4-93cf-da735a10cf2b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Confirm']/parent::*</value>
      <webElementGuid>2d668f1a-1112-4025-aef0-0ed65914cf28</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/form/button</value>
      <webElementGuid>4dbf4c36-b01b-493d-a184-0f6f54c042c5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@type = 'button' and @id = 'confirm' and (text() = 'Confirm' or . = 'Confirm')]</value>
      <webElementGuid>bdbe594e-3a14-4d1a-9e71-97bdd514c32c</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
