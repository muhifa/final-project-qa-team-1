<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Toggle navigation</name>
   <tag></tag>
   <elementGuidId>4f08e046-a6fc-47ca-a2dd-627ebe9fc285</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>button.navbar-toggle.collapsed</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//button[@type='button']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>b5541198-00ba-453b-a1ad-6f241cff1d10</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>e81ee498-42ab-44c2-ae1b-286c4792f6c9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-toggle</name>
      <type>Main</type>
      <value>modal</value>
      <webElementGuid>009510b2-df88-4387-9d5f-a9f40da6ae76</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-target</name>
      <type>Main</type>
      <value>#Modal_user</value>
      <webElementGuid>f7219564-c4cb-4d31-b983-07550e026479</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>navbar-toggle collapsed</value>
      <webElementGuid>fe4d9be2-ab6b-49ee-bd3f-80b1dafd9c76</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-expanded</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>d1fbd2d7-8548-43f3-89c2-86c3e3ab0682</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                        
                                        Toggle navigation
                                        
                                        
                                        
                                        
                                    </value>
      <webElementGuid>250bd873-98e1-47b5-b475-402b6d95d392</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;navbar-col&quot;)/div[@class=&quot;wm-right-section&quot;]/nav[@class=&quot;navbar navbar-default&quot;]/div[@class=&quot;navbar-header&quot;]/button[@class=&quot;navbar-toggle collapsed&quot;]</value>
      <webElementGuid>c270ae64-82d6-4aef-aab4-86af648df45a</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//button[@type='button']</value>
      <webElementGuid>51d4a2c3-6f87-447e-924c-730f55b8a695</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='navbar-col']/div/nav/div/button</value>
      <webElementGuid>29589f13-1263-48cf-9917-3afc35f02211</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Bootcamp'])[1]/preceding::button[3]</value>
      <webElementGuid>d0b7fa19-27e1-4a00-8abf-7f7ecd7d921d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//button</value>
      <webElementGuid>7f57ce9c-a8e3-488a-a5cb-1ce1829abb51</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@type = 'button' and (text() = '
                                        
                                        Toggle navigation
                                        
                                        
                                        
                                        
                                    ' or . = '
                                        
                                        Toggle navigation
                                        
                                        
                                        
                                        
                                    ')]</value>
      <webElementGuid>3d925658-30ed-44d0-9e98-5a044121fea0</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
