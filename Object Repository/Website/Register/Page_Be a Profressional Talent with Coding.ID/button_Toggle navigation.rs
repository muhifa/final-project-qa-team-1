<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Toggle navigation</name>
   <tag></tag>
   <elementGuidId>4d0b561f-339e-4d02-a84c-d9f56cfcc7ec</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//button[@type='button']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>button.navbar-toggle.collapsed</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>2fb7dfd3-35a3-4126-9adf-599e498d2d90</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>c0e72c40-fb6e-4d52-a61e-10471545245f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-toggle</name>
      <type>Main</type>
      <value>modal</value>
      <webElementGuid>f92ebeda-6502-4a27-ad51-170127acdc78</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-target</name>
      <type>Main</type>
      <value>#Modal_user</value>
      <webElementGuid>9955dd7c-3434-47b0-97f0-89690234aec4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>navbar-toggle collapsed</value>
      <webElementGuid>29fa9d6a-b060-4faf-a181-fa67ca502ca2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-expanded</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>efc862c8-cbe8-4817-bcfc-2ab93b56b3d1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                        
                                        Toggle navigation
                                        
                                        
                                        
                                        
                                    </value>
      <webElementGuid>dec8e5a4-551f-4754-b425-18f0f54e964a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;navbar-col&quot;)/div[@class=&quot;wm-right-section&quot;]/nav[@class=&quot;navbar navbar-default&quot;]/div[@class=&quot;navbar-header&quot;]/button[@class=&quot;navbar-toggle collapsed&quot;]</value>
      <webElementGuid>a675e2ed-3e49-48d6-84fe-27cae5149c79</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//button[@type='button']</value>
      <webElementGuid>574b8590-0e8b-4215-8a54-48e7fb06600c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='navbar-col']/div/nav/div/button</value>
      <webElementGuid>8eec9673-5f75-4131-a1ad-86234030af62</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Bootcamp'])[1]/preceding::button[3]</value>
      <webElementGuid>e61eb538-78ed-43ea-a3d9-92c810ffa6ff</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//button</value>
      <webElementGuid>4c9334df-8353-4e2c-b727-a3e5c3070e73</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@type = 'button' and (text() = '
                                        
                                        Toggle navigation
                                        
                                        
                                        
                                        
                                    ' or . = '
                                        
                                        Toggle navigation
                                        
                                        
                                        
                                        
                                    ')]</value>
      <webElementGuid>7f9f1809-208d-44af-90a5-2eacdd6ef29b</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
