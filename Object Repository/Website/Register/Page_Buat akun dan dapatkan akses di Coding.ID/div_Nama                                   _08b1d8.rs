<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Nama                                   _08b1d8</name>
   <tag></tag>
   <elementGuidId>0bfd9673-bdf7-41d6-bf10-198f3f5b1412</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Buat Akun Baru'])[1]/following::div[3]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.col-md-12.col-sm-12.col-xs-12 > div.card-body</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>32ad6617-b205-4a78-8c17-570554bf75f3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>card-body</value>
      <webElementGuid>936b2781-95e7-45e1-b1ee-e2b2f293e2f1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                
                    
                        

                            
                                
                                
                                    Nama

                                    
                                        

                                                                            
                                
                                
                                    Tanggal lahir

                                    
                                        

                                                                            
                                


                                
                                    E-Mail

                                    
                                        

                                                                            
                                
                                
                                    Whatsapp

                                    
                                        

                                                                            
                                

                                
                                    Kata Sandi

                                    
                                        
                                                                            
                                

                                
                                    Konfirmasi kata sandi


                                    
                                        
                                                                            
                                
                                
                                    
                                        
                                        
                                            
                                                Saya setuju
                                                    dengan syarat dan ketentuan yang
                                                    berlaku
                                            
                                        
                                    
                                

                                
                                    
                                        
                                            Daftar
                                        
                                    
                                
                                
                                
                                     
                                            
                                            Sign-in With Google
                                        
                                

                                
                                    Sudah punya akun ?Silahkan
                                        Masuk
                                
                            
                        

                    
                
            </value>
      <webElementGuid>eb41ce1b-769f-4743-831d-809ff03793cc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;no-htmlimports no-flash no-proximity no-applicationcache blobconstructor blob-constructor cookies cors customprotocolhandler dataview eventlistener geolocation history no-ie8compat json notification queryselector serviceworker customevent postmessage svg templatestrings typedarrays websockets no-xdomainrequest webaudio webworkers no-contextmenu cssall audio canvas canvastext contenteditable emoji olreversed no-userdata video no-vml webanimations webgl adownload audioloop canvasblending todataurljpeg todataurlpng todataurlwebp canvaswinding no-ambientlight hashchange inputsearchevent pointerevents no-hiddenscroll mathml unicoderange no-touchevents no-unicode no-batteryapi no-battery-api crypto no-dart gamepads fullscreen indexeddb indexeddb-deletedatabase intl pagevisibility performance pointerlock quotamanagement requestanimationframe raf vibrate no-webintents no-lowbattery getrandomvalues backgroundblendmode cssanimations backdropfilter backgroundcliptext appearance exiforientation no-audiopreload&quot;]/body[1]/div[@class=&quot;wm-main-wrapper&quot;]/div[@class=&quot;wm-main-section&quot;]/div[@class=&quot;row justify-content-center&quot;]/div[@class=&quot;col-md-12 col-sm-12 col-xs-12&quot;]/div[@class=&quot;card-body&quot;]</value>
      <webElementGuid>73a7fe65-1093-4139-94ab-d830de501276</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Buat Akun Baru'])[1]/following::div[3]</value>
      <webElementGuid>bfdc0613-2e99-4f63-a73e-de9c3f3b6823</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/div[2]/div/div</value>
      <webElementGuid>34cb3468-27bb-4000-a349-39cd1a5907a0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                
                    
                        

                            
                                
                                
                                    Nama

                                    
                                        

                                                                            
                                
                                
                                    Tanggal lahir

                                    
                                        

                                                                            
                                


                                
                                    E-Mail

                                    
                                        

                                                                            
                                
                                
                                    Whatsapp

                                    
                                        

                                                                            
                                

                                
                                    Kata Sandi

                                    
                                        
                                                                            
                                

                                
                                    Konfirmasi kata sandi


                                    
                                        
                                                                            
                                
                                
                                    
                                        
                                        
                                            
                                                Saya setuju
                                                    dengan syarat dan ketentuan yang
                                                    berlaku
                                            
                                        
                                    
                                

                                
                                    
                                        
                                            Daftar
                                        
                                    
                                
                                
                                
                                     
                                            
                                            Sign-in With Google
                                        
                                

                                
                                    Sudah punya akun ?Silahkan
                                        Masuk
                                
                            
                        

                    
                
            ' or . = '
                
                    
                        

                            
                                
                                
                                    Nama

                                    
                                        

                                                                            
                                
                                
                                    Tanggal lahir

                                    
                                        

                                                                            
                                


                                
                                    E-Mail

                                    
                                        

                                                                            
                                
                                
                                    Whatsapp

                                    
                                        

                                                                            
                                

                                
                                    Kata Sandi

                                    
                                        
                                                                            
                                

                                
                                    Konfirmasi kata sandi


                                    
                                        
                                                                            
                                
                                
                                    
                                        
                                        
                                            
                                                Saya setuju
                                                    dengan syarat dan ketentuan yang
                                                    berlaku
                                            
                                        
                                    
                                

                                
                                    
                                        
                                            Daftar
                                        
                                    
                                
                                
                                
                                     
                                            
                                            Sign-in With Google
                                        
                                

                                
                                    Sudah punya akun ?Silahkan
                                        Masuk
                                
                            
                        

                    
                
            ')]</value>
      <webElementGuid>64c8bfbc-962e-4ad7-abde-4aaabb9e197c</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
