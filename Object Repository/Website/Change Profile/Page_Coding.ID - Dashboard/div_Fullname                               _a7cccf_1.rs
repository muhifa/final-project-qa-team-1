<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Fullname                               _a7cccf_1</name>
   <tag></tag>
   <elementGuidId>47a0eb50-3a5c-463d-b1a1-8d152a85b789</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='app']/div/div[3]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.main-content</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>b94456c4-d1e7-4165-b7c2-0b89d1229f26</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>main-content</value>
      <webElementGuid>c6ae2015-94d1-440b-9ae7-8a49aa375dc2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                
                    
                        
                            
    
        

            
                
                    

                        
                            
                                                                                                

                                    
                                                                            
                                            
                                            
                                                
                                                
                                            
                                        
                                                                        

                                    
                                    
                                    

                                
                                

                                    
                                        Fullname
                                        
                                        
                                        

                                    

                                                                        
                                        Email
                                        
                                        
                                    
                                    
                                        Phone
                                        
                                        
                                            

                                    
       
                                    
                                        BirthDay
                                        
                                        
                                                                            
                                    
                                        
                                        Cancel
                                        Save Changes
                                    



                                
                            


                        
                    
                
            

        

    
    
                        
                    
                
                                
                    
                        
                            
                                Delete 
                                
                                    ×
                                
                            
                            
                                 
                            
                            
                                
                                                                                                            Delete
                                

                                Close
                            
                        
                    
                
                
                    
                        
                            
                                Withdraw 
                                
                                    ×
                                
                            
                            
                                 
                                
                            
                            

                                Back to My Points
                            
                        
                    
                
                
                    
                        
                            
                                Withdraw Confirmation 
                                
                                    ×
                                
                            
                            
                                 
                            
                            
                                
                                                                        Confirm
                                

                                Cancel
                            
                        
                    
                
                
                    
                        
                        
                    
                
                
                    
                        
                            
                                History 
                                
                                    ×
                                
                            
                            
                                
                                    
                                        
                                            
                                                #
                                                Status
                                                Date
                                                Action By
                                                Notes

                                            
                                        
                                        

                                        
                                    
                                
                            
                            
                                Close
                            
                        
                    
                




            </value>
      <webElementGuid>946ef293-8853-4b71-ac13-a3ef40d54ebc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[@class=&quot;main-wrapper main-wrapper-1&quot;]/div[@class=&quot;main-content&quot;]</value>
      <webElementGuid>c66fcba7-c564-4ea0-a533-96bdadde70b2</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='app']/div/div[3]</value>
      <webElementGuid>92111aa5-961a-4772-9037-ea620b0e0460</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Invoice'])[1]/following::div[1]</value>
      <webElementGuid>14427ee5-092b-4d2d-89f7-658a4f971fdb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div[3]</value>
      <webElementGuid>effe6d0e-f95d-468b-8a6c-c9ca111e4023</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                
                    
                        
                            
    
        

            
                
                    

                        
                            
                                                                                                

                                    
                                                                            
                                            
                                            
                                                
                                                
                                            
                                        
                                                                        

                                    
                                    
                                    

                                
                                

                                    
                                        Fullname
                                        
                                        
                                        

                                    

                                                                        
                                        Email
                                        
                                        
                                    
                                    
                                        Phone
                                        
                                        
                                            

                                    
       
                                    
                                        BirthDay
                                        
                                        
                                                                            
                                    
                                        
                                        Cancel
                                        Save Changes
                                    



                                
                            


                        
                    
                
            

        

    
    
                        
                    
                
                                
                    
                        
                            
                                Delete 
                                
                                    ×
                                
                            
                            
                                 
                            
                            
                                
                                                                                                            Delete
                                

                                Close
                            
                        
                    
                
                
                    
                        
                            
                                Withdraw 
                                
                                    ×
                                
                            
                            
                                 
                                
                            
                            

                                Back to My Points
                            
                        
                    
                
                
                    
                        
                            
                                Withdraw Confirmation 
                                
                                    ×
                                
                            
                            
                                 
                            
                            
                                
                                                                        Confirm
                                

                                Cancel
                            
                        
                    
                
                
                    
                        
                        
                    
                
                
                    
                        
                            
                                History 
                                
                                    ×
                                
                            
                            
                                
                                    
                                        
                                            
                                                #
                                                Status
                                                Date
                                                Action By
                                                Notes

                                            
                                        
                                        

                                        
                                    
                                
                            
                            
                                Close
                            
                        
                    
                




            ' or . = '
                
                    
                        
                            
    
        

            
                
                    

                        
                            
                                                                                                

                                    
                                                                            
                                            
                                            
                                                
                                                
                                            
                                        
                                                                        

                                    
                                    
                                    

                                
                                

                                    
                                        Fullname
                                        
                                        
                                        

                                    

                                                                        
                                        Email
                                        
                                        
                                    
                                    
                                        Phone
                                        
                                        
                                            

                                    
       
                                    
                                        BirthDay
                                        
                                        
                                                                            
                                    
                                        
                                        Cancel
                                        Save Changes
                                    



                                
                            


                        
                    
                
            

        

    
    
                        
                    
                
                                
                    
                        
                            
                                Delete 
                                
                                    ×
                                
                            
                            
                                 
                            
                            
                                
                                                                                                            Delete
                                

                                Close
                            
                        
                    
                
                
                    
                        
                            
                                Withdraw 
                                
                                    ×
                                
                            
                            
                                 
                                
                            
                            

                                Back to My Points
                            
                        
                    
                
                
                    
                        
                            
                                Withdraw Confirmation 
                                
                                    ×
                                
                            
                            
                                 
                            
                            
                                
                                                                        Confirm
                                

                                Cancel
                            
                        
                    
                
                
                    
                        
                        
                    
                
                
                    
                        
                            
                                History 
                                
                                    ×
                                
                            
                            
                                
                                    
                                        
                                            
                                                #
                                                Status
                                                Date
                                                Action By
                                                Notes

                                            
                                        
                                        

                                        
                                    
                                
                            
                            
                                Close
                            
                        
                    
                




            ')]</value>
      <webElementGuid>7e5bba7e-3f6e-47b3-9461-4241a56a5d11</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
