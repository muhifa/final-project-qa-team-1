import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://demo-app.online/')

WebUI.maximizeWindow()

WebUI.click(findTestObject('Object Repository/Page_Beli Event/Page_Be a Profressional Talent with Coding.ID/a_Masuk'))

WebUI.setText(findTestObject('Object Repository/Page_Beli Event/Page_Masuk untuk dapatkan akses di Coding.ID/input_Email_email'), 
    GlobalVariable.Username_iqbal)

WebUI.setEncryptedText(findTestObject('Object Repository/Page_Beli Event/Page_Masuk untuk dapatkan akses di Coding.ID/input_Kata                                 _98da12'), 
    'vOoj4tQjAWVV9mKJ1JcWEw==')

WebUI.click(findTestObject('Object Repository/Page_Beli Event/Page_Masuk untuk dapatkan akses di Coding.ID/button_Login'))

WebUI.click(findTestObject('Object Repository/Page_Beli Event/Page_Be a Profressional Talent with Coding.ID/a_Events'))

WebUI.click(findTestObject('Object Repository/Page_Beli Event/Page_Online event bersertifikat dari prakti_f42b96/h6_Day 3 Predict using Machine Learning'))

WebUI.click(findTestObject('Object Repository/Page_Beli Event/Page_Day 3 Predict using Machine Learning - Ziyad/a_Beli Tiket'))

WebUI.click(findTestObject('Object Repository/Page_Beli Event/Page_Day 3 Predict using Machine Learning - Ziyad/a_Lihat                Pembelian Saya'))

WebUI.click(findTestObject('Object Repository/Page_Beli Event/Page_Coding.ID - Cart/button_Checkout'))

WebUI.click(findTestObject('Object Repository/Page_Beli Event/Page_Coding.ID - Cart/input_Total Pembayaran_payment_method'))

WebUI.click(findTestObject('Object Repository/Page_Beli Event/Page_Coding.ID - Cart/button_Confirm'))

WebUI.click(findTestObject('Object Repository/Page_Beli Event/Page_Coding.ID - Cart/div_OCTO Clicks (1)'))

WebUI.click(findTestObject('Object Repository/Page_Beli Event/Page_Coding.ID - Cart/button_Pay now (2)'))

WebUI.delay(5)

WebUI.verifyTextPresent('Cimb Clicks', false)

WebUI.delay(3)

WebUI.back()

WebUI.click(findTestObject('Page_Beli Event/Page_Be a Profressional Talent with Coding.ID/i_Kontak_fas fa-user-alt'))

WebUI.click(findTestObject('Page_Beli Event/Page_Coding.ID - Invoice/a_My Account'))

WebUI.click(findTestObject('Page_Beli Event/Page_Coding.ID - Dashboard/span_Invoice'))

WebUI.click(findTestObject('Page_Beli Event/Page_Coding.ID - Dashboard/a_Detail'))

WebUI.click(findTestObject('Page_Beli Event/Page_Coding.ID - Dashboard/a_Cancel'))

WebUI.closeBrowser()


