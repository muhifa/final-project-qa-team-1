import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.maximizeWindow()

WebUI.navigateToUrl('https://demo-app.online/')

WebUI.click(findTestObject('Website/Login/button_Masuk_halaman_login'))

WebUI.setText(findTestObject('Object Repository/Website/Login/input_Email_email'), 'muhammadshahidsetiawan@gmail.com')

WebUI.setEncryptedText(findTestObject('Object Repository/Website/Login/input_Kata_sandi'), 'RigbBhfdqOBGNlJIWM1ClA==')

WebUI.click(findTestObject('Object Repository/Website/Login/button_Login'))

WebUI.click(findTestObject('Object Repository/Website/Change Profile/link_icon_user'))

WebUI.click(findTestObject('Object Repository/Website/Change Profile/button_My Account'))

WebUI.click(findTestObject('Object Repository/Website/Change Profile/button__Profil'))

WebUI.click(findTestObject('Object Repository/Website/Change Profile/button_Edit Profile'))

WebUI.setText(findTestObject('Website/Change Profile/input_Phone_whatsapp'), phone)

WebUI.click(findTestObject('Object Repository/Website/Change Profile/button_Save Changes'))

WebUI.closeBrowser()

