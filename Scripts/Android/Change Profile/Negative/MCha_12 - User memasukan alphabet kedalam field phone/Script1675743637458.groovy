import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

String userDir = System.getProperty('user.dir') + '/File_global/'

Mobile.startApplication(userDir + 'DemoAppV2.apk', true)

Mobile.tap(findTestObject('Object Repository/Mobile/Change profile/android.widget.TextView - Login Here'), 0)

Mobile.setText(findTestObject('Object Repository/Mobile/Change profile/android.widget.EditText - Email'), 'jacoby.dagim@foundtoo.com', 
    0)

Mobile.setText(findTestObject('Object Repository/Mobile/Change profile/android.widget.EditText - Password'), 'qwerty123', 
    0)

Mobile.tap(findTestObject('Object Repository/Mobile/Change profile/android.widget.TextView - Login'), 0)

Mobile.delay(3, FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('Mobile/Change profile/android.widget.Button - Profile'), 0)

Mobile.tap(findTestObject('Object Repository/Mobile/Change profile/android.widget.TextView - Setting'), 0)

Mobile.tap(findTestObject('Object Repository/Mobile/Change profile/android.widget.TextView - Edit Profile'), 0)

Mobile.delay(2, FailureHandling.STOP_ON_FAILURE)

Mobile.setText(findTestObject('Mobile/Change profile/android.widget.EditText - CP Number'), '08131989603A', 0)

Mobile.verifyElementText(findTestObject('Mobile/Change profile/android.widget.TextView - number error WhatsApp must be numeric'), 
    'WhatsApp must be numeric')

Mobile.closeApplication()

