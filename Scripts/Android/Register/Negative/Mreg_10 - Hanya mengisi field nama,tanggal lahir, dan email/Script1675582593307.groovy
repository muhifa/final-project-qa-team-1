import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

String userDir = System.getProperty('user.dir') + '/File_global/' 

Mobile.startApplication(userDir + 'DemoAppV2.apk', true)

Mobile.tap(findTestObject('Mobile/Register/button_login here'), 0)

Mobile.tap(findTestObject('Object Repository/Mobile/Register/button_Register, now'), 0)

Mobile.setText(findTestObject('Object Repository/Mobile/Register/input_name'), name, 0)

Mobile.tap(findTestObject('Object Repository/Mobile/Register/button_icon date'), 0)

Mobile.tap(findTestObject('Object Repository/Mobile/Register/click_year 2016'), 0)

Mobile.scrollToText('2005')

Mobile.scrollToText('1999')

Mobile.scrollToText('1996')

Mobile.tap(findTestObject('Object Repository/Mobile/Register/click_year 1996'), 0)

Mobile.tap(findTestObject('Object Repository/Mobile/Register/button_geser kiri month'), 0)

Mobile.tap(findTestObject('Object Repository/Mobile/Register/button_pilih tanggal'), 0)

Mobile.tap(findTestObject('Mobile/Register/button_OK date'), 0)

Mobile.setText(findTestObject('Mobile/Register/input_email'), email, 0)

Mobile.tap(findTestObject('Object Repository/Mobile/Register/button_checkbox'), 0)

Mobile.verifyElementAttributeValue(findTestObject('Mobile/Register/button_daftar_false'), 'enabled', 'false', 0)

Mobile.closeApplication()

