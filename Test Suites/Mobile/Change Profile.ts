<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Change Profile</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>3</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>9d5a6a6d-075a-4ee0-8cc4-dfd60448a91d</testSuiteGuid>
   <testCaseLink>
      <guid>ee2fb44b-9551-43e0-af0d-5b674bd34572</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Android/Change Profile/Negative/MCha_05 - User merubah full name dengan manambahkan format angka</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>4f070d32-1c64-42ad-b381-4f47f8833c20</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Android/Change Profile/Negative/MCha_06 - User merubah full name dengan manambahkan format simbol</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>36e7f19b-5397-40f4-9205-ea203e8e8c74</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Android/Change Profile/Negative/MCha_07 - User merubah full name dengan spasi</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>9aa48111-6f82-4883-8ac1-ec6bc08b3001</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Android/Change Profile/Negative/MCha_09 - User merubah nomor telepon dengan tidak menggunakan format 08</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>8f9ab76a-b7ba-4301-a5b7-9269dc7caea6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Android/Change Profile/Negative/MCha_10 - User merubah nomor telepon dengan menggunakan kurang dari 9 digit angka</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>bba4f297-3fbe-493e-8afe-9a159a78f93c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Android/Change Profile/Negative/MCha_11 - User merubah nomor telepon lebih dari 13 digit angka</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>41a41fca-20e8-4730-88fc-99aee412e623</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Android/Change Profile/Negative/MCha_12 - User memasukan alphabet kedalam field phone</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>af9cf085-1aa9-4b81-92e8-a58850f4c652</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Android/Change Profile/Negative/MCha_13 - User memasukan simpol kedalam field phone</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>eccb49f3-c348-4323-b826-d77b35d85653</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Android/Change Profile/Negative/MCha_14 - User menyisipkan spasi pada nomer telepon</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>21358f07-ef0f-4035-9c47-a58e6a3a7ab1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Android/Change Profile/Negative/MCha_15 - User mengisi field phone dengan spasi sebanyak 9 kali</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>0303e186-f5f0-4b66-8eab-9bdb957e6dbc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Android/Change Profile/Negative/MCha_17 - User mengosonkan field Birtdate</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>92d9f40b-635b-48ad-bc4a-eb18bf241d63</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Android/Change Profile/Negative/MCha_18 - User melakukan perubahan kombinasi</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>dde57c37-34f8-43bf-9ccc-416066658be6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Android/Change Profile/Positive/Mcha_04 - User merubah full name sesuai dengan format</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>4f40e057-7f8a-4aea-a614-16bdd3ff5577</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Android/Change Profile/Positive/MCha_08 - User merubah nomor telepon dengan menggunakan format yang benar</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>435d3665-51a8-4aaf-9580-eba24ff99b76</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Android/Change Profile/Positive/MCha_16 - Merubah tanggal lahir</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
</TestSuiteEntity>
