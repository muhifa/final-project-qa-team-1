<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Login Test Suites</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>3</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>8cffeece-d19b-4d01-b123-6ef32d21202d</testSuiteGuid>
   <testCaseLink>
      <guid>ef9e8844-d460-4375-b8fd-34e20bd81a2c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/API/Login/Negative/AL_02 - Login dengan menggunakan email yang tidak terdaftar</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>04303ff2-a2c7-415c-8e9b-6a161668ebef</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/API/Login/Login</testDataId>
      </testDataLink>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId>04303ff2-a2c7-415c-8e9b-6a161668ebef</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>email</value>
         <variableId>9cb1989a-2171-4216-bf14-4945212aad78</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>04303ff2-a2c7-415c-8e9b-6a161668ebef</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>password</value>
         <variableId>bcb84992-a5dc-4d34-acf4-70fe31fb8cdf</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>a495c40d-9bab-4c2c-964f-67b67dc16682</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/API/Login/Positive/AL_01 - Login dengan email yang yang sudah dibuat</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
</TestSuiteEntity>
