<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description>Melakukan manipulasi pada keranjang</description>
   <name>Cart</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>3</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>4524a14d-466f-461a-8684-ed17412e99d0</testSuiteGuid>
   <testCaseLink>
      <guid>40ff3c5a-4bed-41d8-b238-f25052cc0708</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Website/Buy Event/Positive/WCO_5 - Menghapus item yang ada dalam keranjang</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>f577a127-8bcd-41c8-a1d1-41dccae2fa14</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Website/Buy Event/Positive/WCO_6 - Menghapus salah satu item</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>a90ca123-b092-4e94-b706-5484afa3c5e6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Website/Buy Event/Positive/WCO_7 - Memilih salah satu dari dua item</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>69b4308f-ffa5-410e-9166-dafd8a20faa4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Website/Buy Event/Positive/WCO_8 - Memilih seluruh item</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
</TestSuiteEntity>
