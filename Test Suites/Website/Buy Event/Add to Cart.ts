<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description>Menambahkan event kedalam keranjang</description>
   <name>Add to Cart</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>3</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>e7b48a45-3113-461a-b0dd-26e58522f211</testSuiteGuid>
   <testCaseLink>
      <guid>78a78615-1995-48de-b5d1-6f3ba7b49ba0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Website/Buy Event/Negative/WCO_3 - Menambahkan event kedalam cart tanpa login</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>c2728736-1c2c-4b81-9dc2-394a1f7d556b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Website/Buy Event/Positive/WCO_1 - Menambahkan 1 Event</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>e9f5ff1e-b10a-437e-ac1c-e87d1a2bb34d</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>5ed3e8ce-1820-42d1-a90a-bd1f23b03f43</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>d33c2f4d-ce09-456c-b836-3eea2c0a98cf</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Website/Buy Event/Negative/WCO_23 - Menambahkan event status closed</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
</TestSuiteEntity>
